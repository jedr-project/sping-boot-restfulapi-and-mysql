package com.jedr.restapi.service;

import com.jedr.restapi.entity.RestApiEntity;

import java.util.List;

public interface RestApiService {

    public String createData(RestApiEntity restApi);
    public String updateData(RestApiEntity restApi);
    public  String deleteData(Long id);
    List<RestApiEntity> getAllData();
}
