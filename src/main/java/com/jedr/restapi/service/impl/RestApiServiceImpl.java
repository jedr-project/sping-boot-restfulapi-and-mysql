package com.jedr.restapi.service.impl;

import com.jedr.restapi.entity.RestApiEntity;
import com.jedr.restapi.repo.RestApiRepo;
import com.jedr.restapi.service.RestApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RestApiServiceImpl implements RestApiService {

    @Autowired
    RestApiRepo apiRepo;

    @Override
    public String createData(RestApiEntity restApi) {
         apiRepo.save(restApi);
         return "success";
    }

    @Override
    public String updateData(RestApiEntity restApi) {
        apiRepo.save(restApi);
        return "Updated";
    }

    @Override
    public String deleteData(Long id) {
      apiRepo.deleteById(id);
        return "deleted";
    }

    @Override
    public List<RestApiEntity> getAllData() {
        return apiRepo.findAll();
    }
}
